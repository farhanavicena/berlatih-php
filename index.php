	<?php
	require_once('animal.php');
	require_once('Frog.php');
	require_once('Ape.php');

	$sheep = new animal("shaun");

	echo "$sheep->name <br>";
	echo "$sheep->legs <br>";
	echo "$sheep->cold_blooded <br><br>";



	$kodok = new Frog("buduk");

	echo "$kodok->name <br>";
	echo "$kodok->legs <br>";
	echo "$kodok->cold_blooded <br>";
	echo $kodok->jump();
	echo "<br><br>";


	$sungokong = new Ape("Kera Sakti");
	echo "$sungokong->name <br>";
	echo "$sungokong->legs <br>";
	echo "$sungokong->cold_blooded <br>";
	echo $sungokong->yell();



	?>